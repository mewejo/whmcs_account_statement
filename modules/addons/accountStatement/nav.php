<link href="../modules/addons/accountStatement/css/style.css" rel="stylesheet" type="text/css">
<!-- Nav Bar -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav">
                <li class="brand"><?php echo $LANG['accountstatement'];?></li>
                <li<?php if(!isset($_GET['action'])){ ?> class="active"<?php } ?>><a href="addonmodules.php?module=accountStatement" class="selectedtab" style="line-height: normal;"><i class="fa fa-home"></i> <?php echo $LANG['home'];?></a></li>
                <li <?php if(isset($_GET['action']) && $_GET['action']=='emailtemplate'){ ?> class="active"<?php } ?>><a href="addonmodules.php?module=accountStatement&action=emailtemplate" style="line-height: normal;"><i class="fa fa-envelope"></i> <?php echo $LANG['emailtemplate'];?></a></li>
            </ul>
        </div>
    </div>
</div>
