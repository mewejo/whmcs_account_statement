<?php
use Illuminate\Database\Capsule\Manager as Capsule;
function accountStatement_config() {
    $configarray = array(
        "name" => "Account Statement",
        "description" => "This module send all statements to users every month on their anniversary date",
        "version" => "1.0",
        "author" => "<a href='http://michaels.me.uk'>Russ Michaels</a>",
        "fields" => array()
    );
    return $configarray;
}

function accountStatement_activate() {
    
    try {
        Capsule::schema()->create(
            'mod_account_summary_pdf', function ($table) {
            $table->increments('id');
            $table->integer('userid');
            $table->text('title')->nullable();
        }
        );
    } catch (\Exception $e) {
        logActivity("Unable to create mod_account_summary_pdf: {$e->getMessage()}");
    }
    
    try {
        Capsule::schema()->create(
            'mod_account_summary_configuration', function ($table) {            
            $table->text('setting')->nullable();
            $table->text('value')->nullable();
            $table->timestamps();
        }
        );
    } catch (\Exception $e) {
        logActivity("Unable to create mod_account_summary_configuration: {$e->getMessage()}");
    }
    
    $dir = __DIR__ . '/attachments';
    if (!file_exists($dir)) {
        mkdir($dir);
        chmod($dir, 0777);
    } else {
        chmod($dir, 0777);
    }
}

function accountStatement_deactivate() {
    Capsule::schema()->dropIfExists('mod_account_summary_pdf');
    Capsule::schema()->dropIfExists('mod_account_summary_configuration');    
}

function accountStatement_output($vars) {
    $LANG = $vars['_lang'];
   
        global $CONFIG;
        if (isset($CONFIG['SystemSSLURL']) && !empty($CONFIG['SystemSSLURL'])) {
            $baseurl = $CONFIG['SystemSSLURL'];
        } else {
            $baseurl = $CONFIG['SystemURL'];
        }
        $modulelink = $vars['modulelink'];
        require_once __DIR__ . '/class/class.accountsummary.php';
        $accountsummary = new AccountSummary();
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $userid = $_GET['id'];
            $result = $accountsummary->downloadstatement($userid);      // download account statement pdf using admin area
        } else {
            require_once __DIR__ . '/nav.php';
            if (isset($_GET['action']) && $_GET['action'] == 'emailtemplate') {
                require_once __DIR__ . '/emailtemplate.php';
            } else {
                require_once __DIR__ . '/dashboard.php';
            }
        }
}

function accountStatement_clientarea($vars) {
        global $CONFIG;
        if(!empty($CONFIG['SystemSSLURL'])){
            $baseurl = $CONFIG['SystemSSLURL'];
        }  else {
            $baseurl = $CONFIG['SystemURL'];
        }
        $modulelink = $vars['modulelink'];      //  module client area link
        $LANG = $vars['_lang'];             // Addon module language variable
        require_once __DIR__ . '/class/class.accountsummary.php';     // include class file
        $accountsummary = new AccountSummary();                     // create class object
        if (isset($_GET['uid']) && !empty($_GET['uid'])) {
            $userid = $_GET['uid'];
            $accountsummary->downloadstatement($userid);        // download account statement pdf using client area
        }
        if (isset($_POST['action']) && $_POST['action'] == "genrate_client_statement") {     // generate invoice when filter date 
            $statementArr = array();
            $temp = false;
            $userid = $_POST['userid'];
            $startdate = str_replace("/", "-", $_POST['startdate']);
            $enddate = str_replace("/", "-", $_POST['enddate']);
            $invoicetype = $_POST['invoicetype'];
            $dueinvoices = $accountsummary->get_all_invoces($userid, $enddate, $startdate, $invoicetype);

            if (!empty($dueinvoices)) {
                $pdf = new TCPDF();
                require_once 'accountstatementpdf.php';             // generate account statement pdf 
                if ($temp) {
                    $pdffile = $accountsummary->get_attachment($userid);
                    if (!empty($pdffile)) {
                        $moduleURL = $CONFIG['SystemURL'] . '/modules/addons/accountStatement/';
                        $pdfurl = $moduleURL . 'attachments/' . $pdffile;
                        $statementArr['result'] = "success";
                        $statementArr['startdate'] = $_POST['startdate'];
                        $statementArr['enddate'] = $_POST['enddate'];
                        $statementArr['invoicetype'] = $_POST['invoicetype'];
                        $statementArr['url'] = $pdfurl;
                        $statementArr['file'] = $pdffile;
                        $statementArr['downloadlink'] = $modulelink . "&uid=" . $_SESSION['uid'];
                    }
                }
            } else {
                $statementArr['result'] = "norecord";
                $statementArr['startdate'] = $_POST['startdate'];
                $statementArr['enddate'] = $_POST['enddate'];
                $statementArr['message'] = "No outstanding invoices avaliable";
            }
            $smartyArr = array(
                'baseurl' => $CONFIG['SystemURL'],
                'userid' => $_SESSION['uid'],
                'statements' => $statementArr,
            );
        } else {
            $smartyArr = array(
                'baseurl' => $CONFIG['SystemURL'],
                'userid' => $_SESSION['uid'],
            );
        }

        return array(
            'pagetitle' => 'Account Statement',
            'breadcrumb' => array('index.php?m=accountStatement' => 'Account Statement'),
            'templatefile' => 'clienthome',
            'requirelogin' => true,
            'vars' => $smartyArr,
        );

}