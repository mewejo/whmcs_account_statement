version 1.11
 -- fixed issue of multiple "Accout Statement" emails in one month 
 
version 1.10
- fixed issue with manually generated statements using the "all unpaid invoices" setting.
- fixed issue with invoices from different account showing on statement.

version 1.9
- feature : added option to inlude ALL unpaid invoices instatement, rather than just previous month.
- fix: credit applied and refunded transaction issue
- fix: apply 2 decical places to all output

version 1.8
- feature : added ability to enable/disable monthly statement.

version 1.7
- fixed issue with mass pay invoices 
- fixed issue with missing invoices (addons and custom invoices)

FEATURES
========
  
   This account statement modules for WHMCS is used to send account statements to clients.
   Statements can be sent every month on their anniversary date or can be generated manually from admin or client area.
   You can choose what invoices to include on statements: Paid, Unpaid, ALL
   Set papersize and font family for PDF
   Include statements when sending invoices.
   Edit email template sent to clients.


Configuration
=============

 Admin Area:
   Step 1. Upload accountStatement folder to modules/addons.
   Step 2. Login to whmcs admin panel and goto setup > Addon Modules,
          Activate and Configure "Addon Statements" with required permissions.
   Step 3. Go to Addons -> Account Statement to configure email template and PDF options.

New links will be adding in the client billing menus, if you need to add links elsewhere then use as follows.
   Link:  index.php?m=accountStatement 

If you want to change how the statement looks, then edit the file accountstatementpdf.php



=======================================================================================================

If you find this addon useful then feel free to make a donation via https://www.paypal.me/russmichaels

If you make any improvements or bug fixes to this module, please commit them back to the repository for others to use.

